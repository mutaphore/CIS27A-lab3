/*
@author: Dewei Chen 
@date: 1-24-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a3_1.java
@description: This program computes the area of a square (area = side^2) or 
triangle (area = 1/2 * base * height) after prompting the user to type the 
first character of the figure name (t or s). For a triangle the program asks
for dimensions of base and height. For a square it asks for length of a side.
*/

import java.util.Scanner;

public class a3_1 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		String figureType;
		int base;
		int height;
		int side;
		int area;
		
		System.out.print("Enter the type of figure (s or t): ");
		figureType = input.next();
		
		if (figureType.equals("s"))
		{
			System.out.print("Enter the length of a side: ");
			side = input.nextInt();		
			area = side * side;
		}
		else
		{
			System.out.print("Enter the base: ");
			base = input.nextInt();
			System.out.print("Enter the height: ");
			height = input.nextInt();
			area = (int)(base * height * 0.5); 
		}
		
		System.out.print("The area is " + area);
		
	}

}
