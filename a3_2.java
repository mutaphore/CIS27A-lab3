/*
@author: Dewei Chen 
@date: 1-24-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a3_2.java
@description: This program displays a message specifying the educational
level of a student based on his input of number of years in school.
Between 1 to 6 years is elementary, 7-6 years is middle school, 9-12 years
is high school and greater than 12 years is college.
*/

import java.util.Scanner;

public class a3_2 {

	public static void main(String[] args) {
	
		Scanner input = new Scanner(System.in);
		int years;
		
		System.out.print("Enter number of years of schooling: ");
		years = input.nextInt();
		
		if (years <= 0)
			System.out.print("none");
		else if (years >= 1 && years <= 6)
			System.out.print("elementary");
		else if (years >= 7 && years <= 8)
			System.out.print("middle school");
		else if (years >= 9 && years <= 12)
			System.out.print("high school");
		else
			System.out.print("college");
		
	}
	
}
