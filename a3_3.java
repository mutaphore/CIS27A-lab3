/*
@author: Dewei Chen 
@date: 1-24-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a3_3.java
@description: This program asks the user for 2 numbers and 
an operator to be used between them. It then calculates and outputs the 
result using a switch statement.
*/

import java.util.Scanner;

public class a3_3 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int num1;
		int num2;
		String operatorString;
		char operatorChar;
		
		System.out.print("Enter first integer: ");
		num1 = input.nextInt();
		System.out.print("Enter operator (+,-,x,/): ");
		operatorString = input.next();
		System.out.print("Enter second integer: ");
		num2 = input.nextInt();
		
		//convert string to char because switch statement only takes chars
		operatorChar = operatorString.charAt(0);
		
		switch (operatorChar){
			case '+': System.out.printf("Answer is: %d", num1 + num2);
			break;
			case '-': System.out.printf("Answer is: %d", num1 - num2);
			break;
			case 'x': System.out.printf("Answer is: %d", num1 * num2);
			break;
			case '/': System.out.printf("Answer is: %.2f", (double)num1 / num2);
			break;
		}
		
	}
	
}
