/*
@author: Dewei Chen 
@date: 1-24-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a3_4.java
@description: This program calculates the gross and net cost of
a phone call from San Francisco to Newberg, Oregon based on user 
input of call length and call time. The call rate of 0.40/min is 
halved for calls after 6PM but before 8AM. All calls are subject 
to 4% federal tax. Any calls longer than 60 minutes will get an
additional 15% off on top of other discounts/taxes.
*/

import java.util.Scanner;

public class a3_4 {

	public static void main(String[] args) {
		
		final double callRate = 0.40;
		final double taxRate = 0.04;
		Scanner input = new Scanner(System.in);
		double discountFactor = 1;
		double grossCost;
		double netCost;
		
		System.out.print("Enter start time: ");
		int startTime = input.nextInt();
		System.out.print("Enter length of call in minutes: ");
		int callLength = input.nextInt();
		
		//For calls between 6PM and 8AM, discount of 50% is applied
		if (startTime >= 1800 || startTime < 800)
			discountFactor *= 0.5;
		//For calls > 60 minutes, additional 15% discount is applied
		if (callLength > 60)
			discountFactor *= 0.85;
		
		grossCost = callLength * callRate;
		netCost = grossCost * discountFactor * (1 + taxRate);
		
		System.out.printf("gross cost: %.2f\n", grossCost);
		System.out.printf("net cost: %.2f", netCost);
		
	}

}
